<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstatusTrabajador extends Model
{
  protected $table    = 'estatus_trabajadores';
  protected $fillable =  [ 'clave', 'estatus', 'id_historial' ];
}
