<?php
namespace App\Services;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use Maatwebsite\Excel\Facades\Excel;

class ExportaExcel 
{
    public function exportData($nombreArchivo = 'Datos', $titulosEncabezado = array(), $datos = array(), $cellsBgColor = 'A1:N1', $numAutoFilter = 13, $arrayFormat = array()){
        $nombreArchivo = $nombreArchivo.'_'.date('d_m_y').'_'.date('G_i_s');
        
        Excel::create($nombreArchivo, function($excel) use ($datos, $nombreArchivo, $numAutoFilter, $cellsBgColor, $titulosEncabezado, $arrayFormat) {
            $excel->setTitle($nombreArchivo);
            $excel->setCreator('PBM') ->setCompany('PBM');
            $excel->setDescription('Listado de '. $nombreArchivo);
            $excel->sheet('Hoja 1', function($sheet) use ($datos, $numAutoFilter, $cellsBgColor, $titulosEncabezado, $arrayFormat) {
                //Estilo de la hoja
                $sheet->setFontSize(16);
                $sheet->setColumnFormat($arrayFormat);
                $sheet->freezeFirstRow();
                //$sheet->setAutoFilterByColumnAndRow($numAutoFilter);
                //Estilo de las celdas
                $sheet->cells($cellsBgColor, function($cells) {
                    $cells->setBackground('#B3EFFF');
                    //$cells->setColor('#000000');

                });
                //Encabezado
                $sheet->row(1, $titulosEncabezado);

                //LLenado de celdas
               foreach($datos as $index => $dato) {
                    $sheet->row($index+2, $dato);
                }
            });
        })->export('xlsx');
    }
}