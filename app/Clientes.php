<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $table    = 'clientes';
    protected $fillable =  [
		'nombre',
		'clave',
		'descripcion',
		'max_usuarios_activos',
		'usuarios_activos',
		'activo'
    ];
    public $timestamps = false;
}
