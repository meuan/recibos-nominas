<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistroPatronal extends Model
{
	protected $table    = 'registro_patronal';
	protected $fillable =  [
		'clave',
		'nombre',
		'registro_patronal',
		'RFC',
		'sucursal',
		'activo'
	];
}
