<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecibosNomina extends Model
{
    protected $table    = 'recibos';
    protected $fillable =  [
		'tipo_nomina',
		'periodo',
		'registro_patronal',
		'sucursal',
		'consecutivo',
		'fecha_timbrado',
		'clave_empleado',
		'nombre',
		'apellido_paterno',
		'apellido_materno',
		'RFC',
		'estatus'
    ];
}