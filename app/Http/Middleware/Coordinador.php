<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;
use Closure;
use Session;

class Coordinador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected $auth;
    public function __construct(Guard $auth){
      $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if($this->auth->user()->rol_id != 2){
            return redirect()->to('/acceso_denegado');
          } 
          return $next($request);
        return $next($request);
    }
}
