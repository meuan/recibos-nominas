<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RegistroPatronal;
use App\Clientes;

class RegistroPatronalController extends Controller
{
  public function find(Request $request){
    if(!$request->ajax()) return redirect('/main');
    if($request->rol == 1){
      $registro_patronal = RegistroPatronal::orderBy('id', 'desc')->get();
    }
    else{
      $cliente = Clientes::find($request->cliente_id);
      $registro_patronal = RegistroPatronal::where('sucursal','like','%'.$cliente->clave.'%')
                            ->orderBy('clave', 'desc')->get();
    }
    return $registro_patronal;
  }
}
