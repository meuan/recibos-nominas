<?php

namespace App\Http\Controllers;

use Exception;
use App\Clientes;
use App\RecibosNomina;
use App\RegistroPatronal;
use Illuminate\Http\Request;
use App\Services\ExportaExcel;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class RecibosNominaController extends Controller
{
    protected $buscar;
    protected $exportaExcel;
    protected $error = array();

    public function __construct(ExportaExcel $exportaExcel){
        $this->error = [];
        $this->exportaExcel = $exportaExcel;
    }

    public function find(Request $request){
        if(!$request->ajax()) return redirect('/main');

        $query =  DB::table('recibos')
                    ->where('estatus', 'A')
                    ->where('clave_empleado',  $request->clave_empleado);

        if( $request->rol != "1" ){
            $cliente = Clientes::find( $request->cliente_id);          
            $query->where('sucursal', 'ilike', '%'.$cliente->clave.'%');

            if($request->rol != "2" ){
                $query->whereIn('registro_patronal', $request->registro_patronal)
                    ->where("sucursal", "!=", $request->excepcion);
            }  
        }

        $recibos = $query->orderBy('fecha_timbrado','desc')->get();
        return $recibos;
    }

    public function registro_patronal(Request $request){
        $cliente  = Clientes::find($request->cliente_id);
        $query    = DB::table('recibos')->where('estatus', 'A');

        if($request->excepcion != "NA")
            $query->where("sucursal","!=",$request->excepcion);
        else
            $query->where("sucursal","!=","");

        if($request->rol == "2"){
            $query->where("sucursal",'like','%'.$cliente->clave.'%');
        }
        elseif( $request->rol > "2" ){
            $query->where("sucursal",'like','%'.$cliente->clave.'%');
            $query->where("registro_patronal", $request->registro_patronal);
        }

        if(!empty($request->search)){
            $query->where(function ($query) use ($request){
            $query->whereRaw("CONCAT(nombre, ' ',apellido_paterno,' ',apellido_materno) iLIKE ?", '%'.$request->search.'%')
                ->orWhere('clave_empleado', 'iLIKE', '%'.$request->search.'%');
            });
        }

        if(isset($request->empleado_id) && $request->empleado_id != '')
            $query->where("clave_empleado", $request->empleado_id);
        if(isset($request->sucursal_id)  && $request->sucursal_id != '')
            $query->where("sucursal", 'iLIKE', '%'.$request->sucursal_id.'%');
        if(isset($request->periodo_id)  && $request->periodo_id != '')
            $query->where("periodo", $request->periodo_id);
        if(isset($request->patronal_id)  && $request->patronal_id != ''){
            $array = explode(' - ',$request->patronal_id);
            $query->where("registro_patronal", 'iLIKE', '%'.array_key_first($array).'%');
        }
        if(!empty($request->inicio))
            $query->where("fecha_timbrado",">=",$request->inicio);
        if(!empty($request->final))
            $query->where("fecha_timbrado","<=",$request->final." 23:59:00");

        $recibos = $query->orderBy('fecha_timbrado', 'desc')->paginate($request->resultados);
        return $recibos;
    }


    public function comprimir(Request $request){
        $codigo     = 200;
        $response   = array('message' => true);
        $headers    = ['ok'];

        //Crear la carpeta con permisos de escritura
        $carpeta = public_path()."/".$request->carpeta."/";
        if(!File::isDirectory($carpeta)) {
            if(!File::makeDirectory($carpeta, 0777, true, true))
                $response = false;
        }

        foreach($request->archivos as $ruta){
            try{
                $path   = explode("/", $ruta);
                $para   = $carpeta.'/'.end($path);
                $file   = Storage::disk('s3')->get(substr($ruta, 1));
                file_put_contents($para, $file);
            }
            catch(Exception $e){
                $codigo = 202;
                /* array_push($this->error, end($ruta) ); */  
            }
        }

        $archivo_compreso = $carpeta.'.zip';
        $files = glob(public_path($request->carpeta.'/*'));
        \Zipper::make(public_path($archivo_compreso))->add($files)->close();
        $path_final= public_path($archivo_compreso);

        //Elimino la carpeta con los archivos que se comprimieron
        if (file_exists($carpeta)) {
            foreach(scandir($carpeta) as $file) {
                if ('.' === $file || '..' === $file) continue;
                if (is_dir("$carpeta/$file")) rmdir_recursive("$carpeta/$file");
                else unlink("$carpeta/$file");
            }
            rmdir($carpeta);
        }
        if(count($files) > 0){
          return response()->download($path_final,"test.zip", $headers )->deleteFileAfterSend();
        }
        else{
          $headers = ["Ninguno"];
          $codigo = 403;
          return response()->json(["error" => "No se encontro ningun archivo"], $codigo, $headers);
        }
        
    }

    public function errores(Request $request){
        $registros = $this->getErrores($request->clave);
        $titulosEncabezado = [
            'Clave',
            'Empleado',
            'Periodo',
            'Archivo'
        ];
        $formatColumn = [];
        
        return $this->exportaExcel->exportData('No encontrados', $titulosEncabezado, $registros, 'A1:D1', 3);
    }

    private function getErrores($clave){
        $query = DB::table("log_errores")->where("clave","=",$clave)->get();
        $archivos = explode(",", $query[0]->archivos);
        $arrayRegistros= array();
        
        foreach ($archivos as $index => $registro) {
            $d = explode(" ", $registro);
            $r = explode("_", $d[4]);
            
            $empleado = DB::table("recibos")->where("periodo","=",$d[2])
                            ->where('clave_empleado', "=", $r[0])->get();

            $n = isset($empleado[0]->nombre) ? $empleado[0]->nombre : "";
            $p= isset($empleado[0]->apellido_paterno) ? $empleado[0]->apellido_paterno : "";
            $m= isset($empleado[0]->apellido_materno) ? $empleado[0]->apellido_materno : "";
            $nombre = $n." ".$p." ".$m;
            $extencion = explode(".",$r[1]);
            $arrayRegistros[$index] = [
                $r[0],
                $nombre,
                isset($d[2]) ? $d[2] : "",
                isset($extencion[1]) ? strtoupper($extencion[1]) : "",
            ];
        }
        return $arrayRegistros;
    }




    public function parametros_busqueda(Request $request){
        $sucursales = RecibosNomina::select('sucursal')->groupBy('sucursal');
        $patronales = RegistroPatronal::orderBy('id', 'desc')->select("registro_patronal.*", "registro_patronal.id as patronal_id");
        $periodos   = RecibosNomina::distinct() ->select("periodo")->orderBy('periodo', 'asc');

        if($request->rol != 1){
            $cliente = Clientes::where('id', $request->cliente_id)->select("clave")->first();
            $sucursales->where('sucursal','like','%'.$cliente->clave.'%');
            $patronales->where('sucursal','like','%'.$cliente->clave.'%');
            $periodos->where('sucursal','like','%'.$cliente->clave.'%');
        }

        if(isset($request->sucursal_id) && $request->sucursal_id != ''){
            $patronales->where('sucursal','like','%'.$request->sucursal_id.'%');
        }

        return [
            'sucursales'    => $sucursales->get(),
            'patronales'    => $patronales->get(),
            'periodos'      => $periodos->get(),
        ];
    }

    public function descargar(Request $request) {
        try{
            $path   = explode("/", $request->archivo);
            $para   = public_path()."/".end($path);
            $file   = Storage::disk('s3')->get(substr($request->archivo, 1));
            file_put_contents($para, $file);

            return response()->download($para, end($path), [end($path)])->deleteFileAfterSend();
        }
        catch(Exception $e){
            return response()->json(["error" => "No se encontro el archivo"], 403);
        }
    }
}
