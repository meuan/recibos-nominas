<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\RegistroPatronal;
use App\RecibosNomina;
use App\EstatusTrabajador;
use App\Clientes;
use Carbon\Carbon;

class ConfiguracionController extends Controller
{
    
    protected $serverName;
    protected $connectionInfo;
    protected $response;
    protected $anio;
    public function __construct()
    {
        $this->serverName = env('SERVER_SQL');
        $this->connectionInfo = array( "Database"=>"nominas", "UID"=>"sa", "PWD"=>"AdminPB$2019");
        $this->response = array();
        $this->response["resultado"] = array();
        $fecha = Carbon::now();
        $this->anio = $fecha->format('Y');
    }

    public function registro_patronal(Request $request){
        $conn = sqlsrv_connect( $this->serverName, $this->connectionInfo );
        $clientes = Clientes::select("clave")
        ->where("activo","=", "1")
        ->where("id","!=", "1")->get();
        
        $claves = "cfdi.STATUS = 'A' and (cfdi.SUCURSAL = ''";
        foreach($clientes as $valor){
            $claves = $claves." or cfdi.SUCURSAL like '%".$valor->clave."%'";
        }
        
        if( $conn ) {
            $sql = "SELECT DISTINCT LTRIM(RTRIM(p.CLAVE)) 'clave',LTRIM(RTRIM(p.NOMBRE)) 'nombre' , p.REGISTRO_PATRONAL 'registro_patronal', p.RFC 'RFC', cfdi.SUCURSAL 'sucursal'
                    FROM nominas.dbo.recibos_cfdi AS cfdi 
                    INNER JOIN nominas.dbo.empleados emp ON emp.CLAVE = cfdi.CLAVE
                    INNER JOIN nominas.dbo.patronal p ON p.CLAVE = cfdi.CLAVE_REG_PATRONAL
                    WHERE cfdi.FECHA_TIMBRADO > '".$this->anio."-01-01' AND ".$claves.");";
            
            $stmt = sqlsrv_query( $conn, $sql );
            
            while( $row = sqlsrv_fetch_array( $stmt) ) {
                $tmp = array();
                $tmp["clave"] = utf8_encode($row["clave"]);
                $tmp["nombre"] = utf8_encode($row["nombre"]);
                $tmp["registro_patronal"] = utf8_encode($row["registro_patronal"]);
                $tmp["RFC"] = utf8_encode($row["RFC"]);
                $tmp["sucursal"] = utf8_encode($row["sucursal"]);
                array_push($this->response["resultado"], $tmp);
            }
            sqlsrv_close($conn);

            RegistroPatronal::truncate();
            foreach ($this->response["resultado"] as $val) {
                $registro = new RegistroPatronal();
                $registro->clave = $val["clave"];
                $registro->nombre = $val["nombre"];
                $registro->registro_patronal = $val["registro_patronal"];
                $registro->RFC = $val["RFC"];
                $registro->sucursal = $val["sucursal"];
                $registro->save();
            }
            return "Guardado con Exito";

        }else{
           dd(sqlsrv_errors());
            return utf8_encode(sqlsrv_errors());
       }
    }

    public function recibos_nomina(Request $request){
        $conn = sqlsrv_connect( $this->serverName, $this->connectionInfo );
        $clientes = Clientes::select("clave")
        ->where("activo","=", "1")
        ->where("id","!=", "1")->get();
        
        $claves = "cfdi.STATUS = 'A' and (cfdi.SUCURSAL = ''";
        foreach($clientes as $valor){
            $claves = $claves." or cfdi.SUCURSAL like '%".$valor->clave."%'";
        }

        if( $conn ) {
            $sql = "SELECT cfdi.TIPO_NOMINA 'tipo_nomina', cfdi.PERIODO 'periodo', p.REGISTRO_PATRONAL 'registro_patronal', cfdi.SUCURSAL 'sucursal', cfdi.CONSECUTIVO 'consecutivo', 
                    cfdi.FECHA_TIMBRADO 'fecha_timbrado',
                    LTRIM(RTRIM(emp.CLAVE)) 'clave_empleado',  LTRIM(RTRIM(emp.NOMBREN)) 'nombre', LTRIM(RTRIM(emp.NOMBREP)) 'apellido_paterno',LTRIM(RTRIM( emp.NOMBREM)) 'apellido_materno', 
                    emp.RFC 'RFC', cfdi.STATUS 'estatus', LTRIM(RTRIM(p.CLAVE)) 'clave_patronal'
                    FROM nominas.dbo.recibos_cfdi AS cfdi 
                    INNER JOIN nominas.dbo.empleados emp ON emp.CLAVE = cfdi.CLAVE 
                    INNER JOIN nominas.dbo.patronal p ON p.CLAVE = cfdi.CLAVE_REG_PATRONAL
                    WHERE cfdi.FECHA_TIMBRADO > '".$this->anio."-01-01' AND ".$claves.");";
            $stmt = sqlsrv_query( $conn, $sql );
            
            while( $row = sqlsrv_fetch_array( $stmt) ) {
                $tmp = array();
                $tmp["tipo_nomina"] = utf8_encode($row["tipo_nomina"]);
                $tmp["periodo"] = utf8_encode($row["periodo"]);
                $tmp["registro_patronal"] = utf8_encode($row["registro_patronal"]);
                $tmp["sucursal"]  = utf8_encode($row["sucursal"]);
                $tmp["consecutivo"] = utf8_encode($row["consecutivo"]);
                $tmp["fecha_timbrado"] = utf8_encode($row["fecha_timbrado"]);
                $tmp["clave_empleado"] = utf8_encode($row["clave_empleado"]);
                $tmp["nombre"] = utf8_encode($row["nombre"]);
                $tmp["apellido_paterno"] = utf8_encode($row["apellido_paterno"]);
                $tmp["apellido_materno"] = utf8_encode($row["apellido_materno"]);
                $tmp["RFC"] = utf8_encode($row["RFC"]);
                $tmp["estatus"] = utf8_encode($row["estatus"]);
                $tmp["clave_patronal"] = utf8_encode($row["clave_patronal"]);
                array_push($this->response["resultado"], $tmp);
            }
            sqlsrv_close($conn);
            
            RecibosNomina::truncate();
            foreach($this->response["resultado"] as $valor){
                $recibos = new RecibosNomina();
                $recibos->tipo_nomina = $valor["tipo_nomina"];
                $recibos->periodo = $valor["periodo"];
                $recibos->registro_patronal = $valor["registro_patronal"];
                $recibos->sucursal  = $valor["sucursal"];
                $recibos->consecutivo = $valor["consecutivo"];
                $recibos->fecha_timbrado = $valor["fecha_timbrado"];
                $recibos->clave_empleado = $valor["clave_empleado"];
                $recibos->nombre = $valor["nombre"];
                $recibos->apellido_paterno = $valor["apellido_paterno"];
                $recibos->apellido_materno = $valor["apellido_materno"];
                $recibos->RFC = $valor["RFC"];
                $recibos->estatus = $valor["estatus"];
                $recibos->clave_patronal = $valor["clave_patronal"];
                $recibos->save();
            }
            return "Guardado con Exito";
       }else{
            dd(sqlsrv_errors());
            return utf8_encode(sqlsrv_errors());
       }
    }

    public function estatus_trabajadore(Request $request){
        $conn = sqlsrv_connect( $this->serverName, $this->connectionInfo );

        if( $conn ) {
            $sql = ";WITH CTE AS
                    (
                        SELECT a.CLAVE, a.TIPO, a.ID, ROW_NUMBER() OVER(PARTITION BY a.CLAVE ORDER BY a.ID desc) Corr
                        FROM nominas.dbo.historico_trabajador_bajas a
                    )
                    SELECT LTRIM(RTRIM(CLAVE)) 'clave', TIPO 'estatus', ID 'id_historial'
                    FROM CTE WHERE Corr = 1";
            
            $stmt = sqlsrv_query( $conn, $sql );
            
            while( $row = sqlsrv_fetch_array( $stmt) ) {
                $tmp = array();
                $tmp["clave"] = utf8_encode($row["clave"]);
                $tmp["estatus"] = utf8_encode($row["estatus"]);
                $tmp["id_historial"] = utf8_encode($row["id_historial"]);
                array_push($this->response["resultado"], $tmp);
            }
            sqlsrv_close($conn);

            EstatusTrabajador::truncate();
            foreach ($this->response["resultado"] as $val) {
                $registro = new EstatusTrabajador();
                $registro->clave = $val["clave"];
                $registro->estatus = $val["estatus"];
                $registro->id_historial = $val["id_historial"];
                $registro->save();
            }
            return "Guardado con Exito";

       }else{
           dd(sqlsrv_errors());
            return utf8_encode(sqlsrv_errors());
       }
    }
}
