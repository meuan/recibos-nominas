<?php

namespace App\Http\Controllers;

use App\Clientes;
use Illuminate\Http\Request;
use App\User;
use App\Roles;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller
{
  public function find(Request $request){
    if(!$request->ajax()) return redirect('/main');
    $query = DB::table('users');
    $query->join('roles','users.rol_id','=','roles.id');
    $query->join('clientes', 'users.cliente_id', '=', 'clientes.id');
    $query->select("users.*", "roles.nombre as rol_nombre", "clientes.nombre as nombre_cliente");
    $query->where('users.usuario', '!=', 'root');
    if($request->rol != "1"){
        $query->where('users.cliente_id', '=', $request->cliente_id);
    }

      if(!empty($request->search)){
          $query->whereRaw("users.nombre iLIKE ?", '%'.$request->search.'%');
          $query->orWhere('clientes.nombre', 'iLIKE', '%'.$request->search.'%');
      }
    $query->orderBy('users.id', 'desc');
    $usuarios = $query->paginate($request->resultados);
    return $usuarios;
  }

  public function get(Request $request){
    if(!$request->ajax()) return redirect('/main');
    $usuario = User::where('usuario', '=', $request->usuario)->get();
    return $usuario;
  }

  public function store(Request $request){
    $validateUsuariosActivos = true;
      $response = array(
          'status' => false,
          'message' => 'Error',
      );
      $code = 403;
    if($request->rol_id != 1){
      $validateUsuariosActivos = $this->validateMaxUsuarios($request->cliente_id);
    }

    if($validateUsuariosActivos){
        $usuario = new User();
        $usuario->nombre = $request->nombre;
        $usuario->correo = $request->email;
        $usuario->telefono = $request->telefono;
        $usuario->rol_id = $request->rol_id;
        $usuario->cliente_id = $request->cliente_id;
        $usuario->registro_patronal = $request->registro_patronal;
        $usuario->activo = '1';
        $usuario->usuario = $request->usuario;
        $usuario->password = bcrypt($request->password);
        if($request->excepcion != ""){ 
          $usuario->excepcion = $request->excepcion;
        }
        $usuario->save();
        $this->increaseUsuarioActivo($request->cliente_id);
        $response['status'] = true;
        $response['message'] = 'Se ha guardado con exito';
        $code = 200;

    }else{
        $response['status'] = false;
        $response['message'] = 'Ya no puedes agregar usuarios';
    }
    return response()->json($response, $code);
  }

  public function update(Request $request){
    if(!$request->ajax()) return redirect('/main');
    $usuario = User::findOrFail( $request->id);

    $response = array(
        'status' => false,
        'message' => 'Error',
    );
    $code = 403;

    if($request->accion == "delete"){
      $usuario->activo = $request->activo;
      if($request->activo == 1){
          $validateUsuariosActivos = $this->validateMaxUsuarios($request->cliente_id);
          if($validateUsuariosActivos){
              $this->increaseUsuarioActivo($request->cliente_id);
          }else{
              $response['status'] = false;
              $response['message'] = 'Ya no puedes agregar usuarios';
              $code = 200;
              return response()->json($response, $code);
          }
      }elseif ($request->activo == 0){
          $this->reduceUsuarioActivo($request->cliente_id);
      }
    }
    elseif ($request->accion == "update") {
      $usuario->nombre = $request->nombre;
      $usuario->correo = $request->email;
      $usuario->telefono = $request->telefono;
      $usuario->rol_id = $request->rol_id;
      $usuario->cliente_id = $request->cliente_id;
      $usuario->registro_patronal = $request->registro_patronal;
      $usuario->excepcion = $request->excepcion;
    }
    elseif($request->accion == "password"){
      $usuario->password = bcrypt($request->password);
    }
    if($usuario->save()){
        $response['status'] = true;
        $response['message'] = 'Registro actualizado';
        $code = 200;
    }

    return response()->json($response, $code);
  }

  private function validateMaxUsuarios($cliente_id){
      $validate = false;
      $cliente = Clientes::findOrFail($cliente_id);
      if($cliente->usuarios_activos < $cliente->max_usuarios_activos){
          $validate = true;
      }
      return $validate;
  }

  private function reduceUsuarioActivo($clienteId){
      $cliente = Clientes::findOrFail($clienteId);
      if($cliente->usuarios_activos > 0){
          $cliente->usuarios_activos--;
          $cliente->save();
      }
  }

  private function increaseUsuarioActivo($clienteId){
      $cliente = Clientes::findOrFail($clienteId);
      $cliente->usuarios_activos++;
      $cliente->save();
  }
}
