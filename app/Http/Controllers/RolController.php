<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roles;

class RolController extends Controller
{
    public function find(Request $request){
        if(!$request->ajax()) return redirect('/main');
        if($request->rol == 1){
            $roles = Roles::where("activo","=","1")
                        ->orderBy('id', 'desc')->get();
        }
        else{
            $roles = Roles::where("activo","=","1")
                        ->where("id", "!=", "1")
                        ->orderBy('id', 'desc')->get();
        }
        return $roles;
    }
}
