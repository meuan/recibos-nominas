<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Clientes;
use Illuminate\Support\Facades\DB;

class ClientesController extends Controller
{
    public function find(Request $request){
    if(!$request->ajax()) return redirect('/main');
        $query = DB::table('clientes');
        $query->where("id","!=", "1");
        if(!empty($request->search)){
            $query->where('nombre', 'iLIKE', '%'.$request->search.'%');
            $query->orWhere('clave', 'iLIKE', '%'.$request->search.'%');
        }
        $query->orderBy('id', 'desc');

        if(!empty($request->type_request) && $request->type_request === 'catalogo' ){
            $clientes = $query->get();
        }else{
            $clientes = $query->paginate($request->resultados);
        }
        return $clientes;
    }

    public function store(Request $request){
        $cliente = new Clientes();
        $cliente->nombre = $request->nombre;
        $cliente->clave = $request->clave;
        $cliente->descripcion = $request->descripcion;
        $cliente->activo = '1';
        $cliente->max_usuarios_activos = $request->max_usuarios_activos;
        $cliente->save();
        return "Se ha guardado con exito";
    }

    public function update(Request $request){
        if(!$request->ajax()) return redirect('/main');
        $cliente = Clientes::findOrFail( $request->id);  
        
        if($request->accion == "delete"){
          $cliente->activo = $request->activo;
        }
        elseif ($request->accion == "update") {
            $cliente->nombre = $request->nombre;
            $cliente->clave = $request->clave;
            $cliente->max_usuarios_activos = $request->max_usuarios_activos;
            $cliente->descripcion = $request->descripcion;

        }
        $cliente->save();
        return "Se ha guardado con exito";
    }

    public function get(Request $request){
        if(!$request->ajax()) return redirect('/main');
        $cliente = Clientes::findOrFail( $request->id);
        return $cliente;
    }
}
