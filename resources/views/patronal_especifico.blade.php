@extends('layouts.header')
@section('header')         

  <div class="ui-typography">
      <div class="row">
        <template>
          <div class="col-md-12">
          <input type="hidden" value="{{ $registro }}" id="registro_patronal_especifico" />
            <patronal-especifico></patronal-especifico>
          </div>
        </template>
      </div>
  </div>

@endsection