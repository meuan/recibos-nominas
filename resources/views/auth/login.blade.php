@extends('auth.contenido')
@section('login')

<div class="login-form">
  <img class="align-content imagen_login" src="images/logo_banner.png" alt="">
  <form class="form-horizontal was-validated frmRegistro" method="POST" action="{{ route('login')}}">
    {{ csrf_field() }}
    <div class="form-group">
      <label class=" form-control-label">Usuario:</label>
      <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-user"></i></div>
        <input type="text" value="{{old('usuario')}}" autocomplete="off" name="usuario" id="usuario" class="form-control" placeholder="Usuario">
      </div>
      <span class="invalid-feedback" style="display:block">
        <strong> {!!$errors->first('usuario',':message')!!}</strong>
      </span>
    </div>
      
    <div class="form-group mb-4{{$errors->has('password' ? 'is-invalid' : '')}}">
      <label class=" form-control-label">Contraseña:</label>
      <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-unlock-alt"></i></div>
        <input type="password" autocomplete="off" name="password" id="password" class="form-control" placeholder="Password">
      </div>
      <span class="invalid-feedback" style="display:block">
        <strong> {!!$errors->first('password',':message')!!}</strong>
      </span>
    </div>
      
    <div class="social-login-content">
      <div class="social-button">
        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Acceder</button>
      </div>
    </div>
  </form>
</div>
@endsection
