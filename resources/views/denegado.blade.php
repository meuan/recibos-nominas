<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" ccontent="Recibo de nominas">
    <meta name="author" content="Ing. Manuel Euan.">
    <link rel="shortcut icon" href="/images/logo.jpg">
    <meta name="csrf-token" content="{{ csrf_token()}}">
    <title>Nominas</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/plantilla.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/app.css">
 
</head>
<body class="centrado-porcentual">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col col-lg-2">
          <img src="/images/403.svg" alt="">
        </div>
        <div class="col col-lg-4">
          <h1 class="titulo_acceso_denegado">403</h1>
          <h2>ACCESO DENEGADO.</h2>
          <div class="text-md-center">
            <button onclick="location.href = '/main';" type="button" class="btn btn-success" value="">Ir al inicio</button>
          </div>
        </div>
      </div>
    </div>     
  
</body>
</html>
