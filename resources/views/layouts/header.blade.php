<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" ccontent="Sistema para descargar recibo de nominas.">
    <meta name="author" content="Ing. Manuel Euan. && Ing. Jorge Mena">
    <link rel="shortcut icon" href="/images/logo_nuevo.jpg">
    <meta name="csrf-token" content="{{ csrf_token()}}">
    <title>Nominas</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/plantilla.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/app.css">
  </head>
    <body>
      <div id="app">
        <aside id="left-panel" class="left-panel">
          <nav class="navbar navbar-expand-sm navbar-default">
            @include('sidebar.administrador')
          </nav>
        </aside>
        <div id="right-panel" class="right-panel"> 
          <header id="header" class="header">
            <div class="top-left">
              <div class="navbar-header">
                <a class="navbar-brand" href="/main"><img src="/images/logo_banner.png" alt="Logo" class="logo_empresa"></a>
                <a class="navbar-brand hidden" href="./">
                  <img src="/images/logo_banner.png" alt="Logo" >
                </a>
                <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
              </div>
            </div>
            <div class="top-right">
              <div class="header-menu">
                <div class="user-area dropdown float-right">
                  <a href="#" class="dropdown-toggle active nombre_usuario" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Bienvenido : {{ Auth::user()->nombre }} &nbsp;&nbsp;
                    <img class="user-avatar rounded-circle" src="/images/avatar/usuario.png" alt="User Avatar">
                  </a>
                  <div class="user-menu dropdown-menu" style="right:-2px !important;">
                    <a class="nav-link" href="{{ route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form).submit();">
                      <i class="fa fa-power-off"></i>Salir
                    </a>
                    <form action="{{route('logout')}}" method="POST" id="logout-form" style="display:none;">
                      {{ csrf_field() }}
                    </form>
                    <input type="hidden" name="registro_patronal" id="registro_patronal" value="{{ Auth::user()->registro_patronal }}">
                    <input type="hidden" name="rol_usuario" id="rol_usuario" value="{{ Auth::user()->rol_id }}">
                    <input type="hidden" name="cliente_id" id="cliente_id" value="{{ Auth::user()->cliente_id }}">
                    <input type="hidden" name="excepcion" id="excepcion" value="{{ Auth::user()->excepcion }}">
                  </div>
                </div>
              </div>
            </div>
          </header>
          
          <div class="content">
            <div class="animated fadeIn">
              @yield('header')
            </div>
          </div>
        
        </div>
    </div>
      
  <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
  <script src="/js/all.js"></script>
  <script src="/js/app.js"></script>
</body>
</html>