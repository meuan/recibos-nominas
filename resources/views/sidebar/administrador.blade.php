<div id="main-menu" class="main-menu collapse navbar-collapse">
  <ul class="nav navbar-nav">
    @if(Auth::user()->rol_id == 4)
      <li>
        <a href="/main" style="text-decoration:none"><i class="menu-icon fa fa-laptop"></i>Inicio </a>
      </li>

    @elseif(Auth::user()->rol_id == 3 )
      <li>
        <a href="/main" style="text-decoration:none"><i class="menu-icon fa fa-laptop"></i>Inicio </a>
      </li>
      <li class="menu-title">Registro Patronal</li>
      

      <?php
        $cadena     =  Auth::user()->registro_patronal;
        $array_menu = explode(",", $cadena);
        $array_menu = array_unique($array_menu);
      ?>

      @foreach( $array_menu as $item)
          <li>
            <a href="/registros_especificos/{{ $item }}" style="text-decoration:none"><i class="menu-icon fa fa-sitemap"></i> {{ $item }} </a>
          </li>
      @endforeach 


    @elseif( Auth::user()->rol_id == 2)
      <li>
        <a href="/main" style="text-decoration:none"><i class="menu-icon fa fa-laptop"></i>Inicio </a>
      </li>
      <li class="menu-title">Recibos Nómina</li>
      <li>
        <a href="/registro_patronal" style="text-decoration:none"><i class="menu-icon fa fa-sitemap"></i>Registro patronal </a>
      </li>  
      <li class="menu-title">Configuración</li>
      <li>
          <a href="/usuarios" style="text-decoration:none"> <i class="menu-icon fa fa-users" title="Usuarios"></i>Usuarios </a>
      </li>
      
      @elseif( Auth::user()->rol_id == 1)
      <li>
        <a href="/main" style="text-decoration:none"><i class="menu-icon fa fa-laptop"></i>Inicio </a>
      </li>
      <li class="menu-title">Recibos Nómina</li>
      <li>
        <a href="/registro_patronal" style="text-decoration:none"><i class="menu-icon fa fa-sitemap"></i>Registro patronal </a>
      </li>  
      <li class="menu-title">Configuración</li>
      <li>
          <a href="/usuarios" style="text-decoration:none"> <i class="menu-icon fa fa-users" title="Usuarios"></i>Usuarios </a>
      </li>
      <li>
        <a href="/clientes" style="text-decoration:none"> <i class="menu-icon fa fa-users" title="Usuarios"></i>Cientes</a>
      </li>  
      {{-- <li>
          <a href="/update_database" style="text-decoration:none"> <i class="menu-icon fa fa-gears (alias)" title="Usuarios"></i>Base de datos </a>
      </li>    --}}   
    @endif
  </ul>
  
</div>