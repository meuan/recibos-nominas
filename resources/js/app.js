/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.BootstrapVue = require('bootstrap-vue');
import BootstrapVue from 'bootstrap-vue'


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('recibos-nomina', require('./components/RecibosComponent.vue'));
Vue.component('usuarios-nominas', require('./components/UsuariosComponent.vue'));
Vue.component('registro-patronal', require('./components/RegistroPatronalComponent.vue'));
Vue.component('update-database', require('./components/UpdateDataBaseComponent.vue'));
Vue.component('patronal-especifico', require('./components/PatronalEspecificoComponent.vue'));
Vue.component('clientes-sistema', require('./components/ClientesComponent.vue'));

Vue.use(BootstrapVue);

const app = new Vue({
    el: '#app'
});