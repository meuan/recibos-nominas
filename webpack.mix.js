const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
   'resources/plantilla/style.css',  
   'resources/plantilla/miCss.css', 
], 'public/css/plantilla.css')
.scripts([
   'resources/plantilla/main.js',
   'resources/plantilla/sweetalert2.js'
], 'public/js/all.js')
.js('resources/js/app.js', 'public/js')
.sass('resources/sass/app.scss', 'public/css');
