<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

   
Route::post('/comprimir', 'RecibosNominaController@comprimir');
Route::post('/descarga_excel', 'RecibosNominaController@errores');

/* Route::group(['prefix' => 'usuarios'], function () {
    Route::get('/find', 'UsuariosController@find');
    Route::get('/', 'UsuariosController@get');
    Route::post('/', 'UsuariosController@store');
    Route::put('/', 'UsuariosController@update');
});

Route::group(['prefix' => 'clientes'], function () {
    Route::get('/find', 'ClientesController@find');
    Route::get('/', 'ClientesController@get');
    Route::post('/', 'ClientesController@store');
    Route::put('/', 'ClientesController@update');
});

Route::group(['prefix' => 'registro_patronal'], function () {
    Route::get('/find', 'RegistroPatronalController@find');
});

Route::group(['prefix' => 'recibos_nomina'], function () {
    Route::get('/find', 'RecibosNominaController@find');
    Route::get('/registro_patronal', 'RecibosNominaController@registro_patronal');
    Route::get('/', 'RecibosNominaController@get');

    Route::get('/parametros_busqueda', 'RecibosNominaController@parametros_busqueda');
    Route::get('/busqueda_general', 'RecibosNominaController@busqueda_general');
    Route::post('/comprimir', 'RecibosNominaController@comprimir');
});

Route::group(['prefix' => 'roles'], function () {
    Route::get('/find', 'RolController@find');
});

Route::group(['prefix' => 'update_database'], function () {
    Route::get('/registro_patronal', 'ConfiguracionController@registro_patronal');
    Route::get('/recibos_nomina', 'ConfiguracionController@recibos_nomina');
    Route::get('/estatus_trabajadore', 'ConfiguracionController@estatus_trabajadore');    
}); */
