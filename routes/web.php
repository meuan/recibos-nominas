<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['guest']], function () {
    Route::get('/','Auth\LoginController@showLoginForm');
    Route::get('/login','Auth\LoginController@showLoginForm');
    Route::post('/login', 'Auth\LoginController@login')->name('login');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/acceso_denegado', function () { return view('denegado'); })->name("denegado");
    Route::get('/main', function () { return view('index'); })->name("main");
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');    
    Route::get('/registro_patronal', function () { return view('registro_patronal'); });
    
    Route::get('/registros_especificos/{registro}', function ($registro = null) { 
        return view('patronal_especifico')->with('registro',$registro);;  
    });
    Route::get('/usuarios', function () { return view('usuarios'); });

    Route::group(['middleware' => ['administrador']], function () {
        Route::get('/update_database', function () { return view('update_database'); });
        Route::get('/clientes', function () { return view('clientes'); });
    });

    Route::group(['prefix' => 'api/usuarios'], function () {
        Route::get('/find', 'UsuariosController@find');
        Route::get('/', 'UsuariosController@get');
        Route::post('/', 'UsuariosController@store');
        Route::put('/', 'UsuariosController@update');
    });

    Route::group(['prefix' => 'api/clientes'], function () {
        Route::get('/find', 'ClientesController@find');
        Route::get('/', 'ClientesController@get');
        Route::post('/', 'ClientesController@store');
        Route::put('/', 'ClientesController@update');
    });

    Route::group(['prefix' => 'api/registro_patronal'], function () {
        Route::get('/find', 'RegistroPatronalController@find');
    });

    Route::group(['prefix' => 'api/recibos_nomina'], function () {
        Route::get('/find', 'RecibosNominaController@find');
        Route::get('/registro_patronal', 'RecibosNominaController@registro_patronal');
        Route::get('/', 'RecibosNominaController@get');

        Route::get('/parametros_busqueda', 'RecibosNominaController@parametros_busqueda');
        Route::post('/comprimir', 'RecibosNominaController@comprimir');
        Route::post('/descargar', 'RecibosNominaController@descargar');
        Route::post('/descarga_excel', 'RecibosNominaController@errores');
    });

    Route::group(['prefix' => 'api/roles'], function () {
        Route::get('/find', 'RolController@find');
    });

    Route::group(['prefix' => 'api/update_database'], function () {
        Route::get('/registro_patronal', 'ConfiguracionController@registro_patronal');
        Route::get('/recibos_nomina', 'ConfiguracionController@recibos_nomina');
        Route::get('/estatus_trabajadore', 'ConfiguracionController@estatus_trabajadore');    
    });
});