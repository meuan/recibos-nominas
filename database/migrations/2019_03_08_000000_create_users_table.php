<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nombre',100);
      $table->string('correo',100);
      $table->string('telefono',25)->nullable();
      $table->integer('rol_id')->unsigned();
      $table->integer('cliente_id')->unsigned();
      $table->string('registro_patronal',250)->nullable();
      $table->boolean('activo')->default('1');
      $table->string('usuario',50);
      $table->string('password');
      $table->rememberToken();
      
      $table->foreign('rol_id')->references('id')->on('roles');
      $table->foreign('cliente_id')->on()->references('id')->on('clientes');
    });

    DB::table('users')->insert(array(
      'nombre' => 'Administrador del sistema',
      'correo' => 'administrador@interno.com.mx',
      'telefono' => '0000000000',
      'rol_id' => 1,
      'cliente_id' => 1,
      'activo' => '1',
      'usuario' => 'root',
      'password' => bcrypt('aV@w4@2gAcp&')
    ));
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
