<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistroPatronalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_patronal', function (Blueprint $table) {
            $table->increments('id');
            $table->string('clave',20);
            $table->string('nombre',250)->nullable();
            $table->string('registro_patronal',25)->nullable();
            $table->string('RFC',20)->nullable();
            $table->string('sucursal',50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_patronal');
    }
}
