<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',50);
            $table->string('descripcion',250)->nullable();
            $table->boolean('activo')->default('1');
        });
        DB::table('roles')->insert(array(
            'nombre' => 'SUPER USUARIO',
            'descripcion' => 'Usuario con todo los permisos disponibles.',
            'activo' => '1'
        ));

        DB::table('roles')->insert(array(
            'nombre' => 'ADMINISTRADOR',
            'descripcion' => 'Usuario con permisos para la creación de los usuarios.',
            'activo' => '1'
        ));
        DB::table('roles')->insert(array(
            'nombre' => 'COORDINADOR',
            'descripcion' => 'Usuario con permisos para realizar la búsqueda individual u/o por registro patronal.',
            'activo' => '1'
        ));
        DB::table('roles')->insert(array(
            'nombre' => 'TRABAJADOR',
            'descripcion' => 'Usuarios que solo puede buscar individualmente.',
            'activo' => '1'
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
