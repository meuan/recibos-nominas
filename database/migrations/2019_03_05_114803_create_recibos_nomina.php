<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecibosNomina extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recibos_nomina', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo_nomina',250)->nullable();
            $table->string('periodo',10)->nullable();
            $table->string('registro_patronal',50)->nullable();
            $table->string('sucursal',50)->nullable();
            $table->string('consecutivo',5);
            $table->date('fecha_timbrado');
            $table->string('clave_empleado',25);
            $table->string('nombre',50);
            $table->string('apellido_paterno',50);
            $table->string('apellido_materno',50)->nullable();
            $table->string('RFC',20);
            $table->string('estatus',5);
            $table->string('clave_patronal',20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recibos_nomina');
    }
}
