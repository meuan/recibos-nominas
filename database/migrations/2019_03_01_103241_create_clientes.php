<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',50);
            $table->string('clave',50);
            $table->string('descripcion',250)->nullable();
            $table->boolean('activo')->default('1');
        });
        DB::table('clientes')->insert(array(
            'nombre' => 'Pb Management',
            'clave' => 'todo',
            'descripcion' => 'Empresa administradora del sistema.',
            'activo' => '1'
        ));
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
