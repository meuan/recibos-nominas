<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServidorRegistroPatronal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('registro_patronal', function (Blueprint $table) {
            $table->string('servidor',10)->nullable()->default("NA");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('registro_patronal', function (Blueprint $table) {
            $table->dropColumn('servidor');
        });
    }
}
