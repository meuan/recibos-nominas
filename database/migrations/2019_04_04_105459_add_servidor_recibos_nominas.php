<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServidorRecibosNominas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recibos_nomina', function (Blueprint $table) {
            $table->string('servidor',10)->nullable()->default("NA");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recibos_nomina', function (Blueprint $table) {
            $table->dropColumn('servidor');
        });
    }
}
